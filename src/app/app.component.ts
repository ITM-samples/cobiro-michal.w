import {Component, OnInit} from '@angular/core';
import {TeamService} from '../team-members/service/team.service';
import {Observable} from 'rxjs';
import {TeamMembers} from '../team-members/model/model.types';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  title = 'Team members';

  teamMembers$: Observable<TeamMembers>;

  constructor(private teamService: TeamService) {
  }

  ngOnInit(): void {
    this.teamMembers$ = this.teamService.getTeamMembers();
  }

}
