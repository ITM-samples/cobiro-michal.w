export interface TeamMember {
  name: string;
  position: string;
  email: string;
  phone: string;
  image: string;
}

export interface TeamMembers {
  members: Array<TeamMember>;
  title: string;
}
