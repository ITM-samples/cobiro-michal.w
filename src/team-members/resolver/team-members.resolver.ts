import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {TeamService} from '../service/team.service';
import {TeamMembers} from '../model/model.types';

@Injectable({ providedIn: 'root' })
export class TeamMembersResolver implements Resolve<TeamMembers> {
  constructor(private service: TeamService) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<any>|Promise<any>|any {
    return this.service.getTeamMembers();
  }
}
