export interface ImageUrl {
  'w200': string;
  'w400': string;
  'w1080': string;
  'w1920': string;
  'w2560': string;
}

export interface Block {
  title: string;
  description: string;
  link: string;
  text: string;
}

export interface Member {
  imageUrl: ImageUrl;
  block: Block;
}

export interface MembersCards {
  first: Member;
  second: Member;
  third: Member;
}

export interface Attribute {
  title: string;
  memberCards: MembersCards;
}

export interface ResponseItem {
  type: string;
  id: number;
  attributes: Attribute;
}

export interface OurTeamResponse {
  data: Array<ResponseItem>;
}
