import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {OurTeamResponse} from './api.types';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiClient {

  constructor(private httpClient: HttpClient) {
  }

  getTeamMembers(): Observable<OurTeamResponse> {
    return this.httpClient.get<OurTeamResponse>(environment.apiTeamMembersUrl);
  }

}
