import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Observable} from 'rxjs';
import {TeamMembers} from '../../model/model.types';
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-team-members',
  templateUrl: './team-members.component.html',
  styleUrls: ['./team-members.component.scss']
})
export class TeamMembersComponent implements OnInit {
  teamMembers$: Observable<TeamMembers>;

  constructor(private activateRoute: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.teamMembers$ = this.activateRoute.data
      .pipe(map(value => value.teamMembers));
  }

}
