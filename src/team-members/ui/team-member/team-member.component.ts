import {Component, Input, OnInit} from '@angular/core';
import {TeamMember} from '../../model/model.types';

@Component({
  selector: 'app-team-member',
  templateUrl: './team-member.component.html',
  styleUrls: ['./team-member.component.scss']
})
export class TeamMemberComponent implements OnInit {

  @Input()
  member: TeamMember;

  constructor() { }

  ngOnInit(): void {
  }

}
