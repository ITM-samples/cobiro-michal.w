import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {TeamMembersGridComponent} from './ui/team-members-grid/team-members-grid.component';
import {TeamMemberComponent} from './ui/team-member/team-member.component';
import {TeamMembersComponent} from './ui/team-members/team-members.component';
import {ApiClient} from './api/api.client';
import {TeamService} from './service/team.service';
import {TeamMembersResolver} from './resolver/team-members.resolver';
import {RouterModule} from '@angular/router';



@NgModule({
  declarations: [
    TeamMembersGridComponent,
    TeamMemberComponent,
    TeamMembersComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: TeamMembersComponent,
        resolve: {
          teamMembers: TeamMembersResolver
        },
      }
    ])
  ],
  providers: [
    ApiClient,
    TeamService,
    TeamMembersResolver
  ],
})
export class TeamMembersModule { }
