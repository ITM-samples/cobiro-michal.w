import {Component, Input, OnInit} from '@angular/core';
import {TeamMembers} from '../../model/model.types';

@Component({
  selector: 'app-team-members-grid',
  templateUrl: './team-members-grid.component.html',
  styleUrls: ['./team-members-grid.component.scss']
})
export class TeamMembersGridComponent implements OnInit {

  @Input()
  teamMembers: TeamMembers;

  constructor() {
  }

  ngOnInit(): void {
  }

}
