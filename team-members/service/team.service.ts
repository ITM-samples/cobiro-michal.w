import {Injectable} from '@angular/core';
import {ApiClient} from '../api/api.client';
import {Observable} from 'rxjs';
import {TeamMember, TeamMembers} from '../model/model.types';
import {map} from 'rxjs/operators';
import {Member} from '../api/api.types';

@Injectable({
  providedIn: 'root'
})
export class TeamService {

  constructor(private apiClient: ApiClient) {
  }

  getTeamMembers(): Observable<TeamMembers> {
    return this.apiClient.getTeamMembers().pipe(map(response => {
        const attributes = response.data[0].attributes;
        const title = attributes.title;
        const members = [
          attributes.memberCards.first,
          attributes.memberCards.second,
          attributes.memberCards.third
        ].map(this.mapMember);
        return {
          title,
          members
        };
      }
    ));
  }

  mapMember(member: Member): TeamMember {
    return {
      email: member.block.link,
      name: member.block.title,
      phone: member.block.text,
      position: member.block.description,
      image: member.imageUrl.w200
    };
  }

}
