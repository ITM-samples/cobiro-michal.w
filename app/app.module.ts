import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {HttpClientModule} from '@angular/common/http';
import {RouterModule} from '@angular/router';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot([
      {
        path: 'team-members',
        loadChildren: () => import('../team-members/team-members.module').then(m => m.TeamMembersModule)
      },
      {
        path: '**',
        redirectTo: 'team-members'
      }
    ])
  ],
  bootstrap: [AppComponent],
  exports: [RouterModule]
})
export class AppModule {
}


